using System;
using Xunit;
using Katas.Kata_6;
namespace KataUnitTests
{
    public class PigLatinTranslatorTests
    {
        [Fact]
        public void TranslateWord_flag_shouldReturnString_agflayQuestionMArk() {
            Assert.Equal("agflay?", PigLatinTranslator.TranslateWord("flag?"));
         }
        [Fact]
        public void TranslateWord_flag_shouldReturnString_agflay()
        {
            Assert.Equal("oneyhay", PigLatinTranslator.TranslateWord("honey"));
        }
        [Fact]
        public void TranslateSentence_Iliketoeathoneywaffles_shouldWork()
        {
            Assert.Equal("Iay ikelay otay eatay oneyhay afflesway. ", PigLatinTranslator.TranslateSentence("I like to eat honey waffles."));
        }

    }
}
