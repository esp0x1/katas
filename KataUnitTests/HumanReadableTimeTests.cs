using System;
using Xunit;
using Katas.Kata_2;
namespace KataUnitTests
{
    public class HumanReadableTimeTests
    {
        [Fact]
        public void getHumanReadableTime_int0_shouldReturn00h00m00s()
        {
            string expected = "00:00:00";
            int input = 0;

            HumanReadableTime myTimeObj = new HumanReadableTime();
            string actual = myTimeObj.GetReadableTime(input);

            Assert.Equal(expected, actual);
        }        
        
        [Fact]
        public void getHumanReadableTime_int5_shouldReturn00h00m05s()
        {
            string expected = "00:00:05";
            int input = 5;

            HumanReadableTime myTimeObj = new HumanReadableTime();
            string actual = myTimeObj.GetReadableTime(input);

            Assert.Equal(expected, actual);
        }        
        [Fact]
        public void getHumanReadableTime_int60_shouldReturn00h01m00s()
        {
            string expected = "00:01:00";
            int input = 60;

            HumanReadableTime myTimeObj = new HumanReadableTime();
            string actual = myTimeObj.GetReadableTime(input);

            Assert.Equal(expected, actual);
        }        
        [Fact]
        public void getHumanReadableTime_int86399_shouldReturn23h59m59s()
        {
            string expected = "23:59:59";
            int input = 86399;

            HumanReadableTime myTimeObj = new HumanReadableTime();
            string actual = myTimeObj.GetReadableTime(input);

            Assert.Equal(expected, actual);
        }        
        [Fact]
        public void getHumanReadableTime_int359999_shouldReturn99h59m59s()
        {
            string expected = "99:59:59"; // yep
            int input = 359999;

            HumanReadableTime myTimeObj = new HumanReadableTime();
            string actual = myTimeObj.GetReadableTime(input);


            Assert.Equal(expected, actual);
        }
    }
}
