﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Katas.Kata_3;
using Xunit;

namespace KataUnitTests
{
    public class RmLetterTests
    {
        [Fact]
        public void RemoveLetters_ArrayOfStringwAndRemoveString_shouldResultInArrayWithW()
        {
            string[] expected = new[] { "w" };
            string[] toEdit = new[] { "s", "t", "r", "i", "n", "g", "w" };
            string toRemove = "string";

            RemoveTheWord sm = new RemoveTheWord();
            string[] actual = sm.RemoveLetters(toEdit, toRemove);

            Assert.Equal(expected, actual);
        }
        [Fact]
        public void RemoveLetters_ArrayOfBBLLGNOAWAndRemoveBALLOON_shouldResultInArrayWithBGW()
        {
            string[] expected = new[] { "b","g","w" };
            string[] toEdit = new[] { "b","b", "l", "l", "g", "n", "o", "a","w" };
            string toRemove = "balloon";

            RemoveTheWord sm = new RemoveTheWord();
            string[] actual = sm.RemoveLetters(toEdit, toRemove);

            Assert.Equal(expected, actual);
        }
        [Fact]
        public void RemoveLetters_ArrayOfANRYOWAndRemoveNORWAY_shouldResultInEmptyArray()
        {
            string[] expected = new string[] { };
            string[] toEdit = new[] { "a", "n", "r", "y","o", "w" };
            string toRemove = "norway";

            RemoveTheWord sm = new RemoveTheWord();
            string[] actual = sm.RemoveLetters(toEdit, toRemove);

            Assert.Equal(expected, actual);
        }
        [Fact]
        public void RemoveLetters_ArrayOfTTESTUAndRemoveTESTING_shouldResultInTU()
        {
            string[] expected = new string[] { "t", "u" };
            string[] toEdit = new[] { "t", "t", "e", "s", "t", "u" };
            string toRemove = "testing";

            RemoveTheWord sm = new RemoveTheWord();
            string[] actual = sm.RemoveLetters(toEdit, toRemove);

            Assert.Equal(expected, actual);
        }
        [Fact]
        public void RemoveLetters_ArrayOfTTESTUAndRemoveNothing_shouldResultInttestu()
        {
            string[] expected = new string[] { "t", "t", "e", "s", "t", "u" };
            string[] toEdit = new[] { "t", "t", "e", "s", "t", "u" };
            string toRemove = "";

            RemoveTheWord sm = new RemoveTheWord();
            string[] actual = sm.RemoveLetters(toEdit, toRemove);

            Assert.Equal(expected, actual);
        }
        [Fact]
        public void RemoveLetters_ArrayEmptyAndRemoveNothing_shouldResultInEmptyArray()
        {
            string[] expected = new string[] {};
            string[] toEdit = new string[] {};
            string toRemove = "";

            RemoveTheWord sm = new RemoveTheWord();
            string[] actual = sm.RemoveLetters(toEdit, toRemove);

            Assert.Equal(expected, actual);
        }

    }
}
