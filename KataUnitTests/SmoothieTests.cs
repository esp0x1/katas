using System;
using Xunit;
using Katas.Kata_5;
namespace KataUnitTests
{
    public class SmoothiesTests
    {
        [Fact]
        public void getCost_BananaAndSoy_shouldReturnDoubleZeroPointFive() {
            Smoothie s1 = new(new string[] { "Banana", "Soy" });

            Assert.Equal(0.5, s1.GetCost());
         }
        [Fact]
        public void getPrice_BananaAndSoy_shouldReturnDoubleOnePointTwentyFive()
        {
            Smoothie s1 = new(new string[] { "Banana", "Soy" });

            Assert.Equal(1.25, s1.GetPrice());
        }
        [Fact]
        public void getName_BananaAndSoy_shouldReturnStringBananaSmoothieVGN()
        {
            Smoothie s1 = new(new string[] { "Banana", "Soy" });

            Assert.Equal("Banana Smoothie (VGN)", s1.GetName());
        }
        [Fact]
        public void getCost_RaspberryBlueBerryStrawberryCow_shouldReturnThreePointFive()
        {
            Smoothie s1 = new(new string[] { "Raspberry", "Strawberry", "Blueberry", "Cow" });

            Assert.Equal(3.5, s1.GetCost());
        }
        [Fact]
        public void getPrice_RaspberryBlueBerryStrawberryCow_shouldReturnEightPointSeventyFive()
        {
            Smoothie s1 = new(new string[] { "Raspberry", "Strawberry", "Blueberry", "Cow" });

            Assert.Equal(8.75, s1.GetPrice());
        }
        [Fact]
        public void getPrice_RaspberryBlueBerryStrawberryCow_shouldReturnStringBlueberryRaspberryStrawberryFusion()
        {
            Smoothie s1 = new(new string[] { "Raspberry", "Strawberry", "Blueberry", "Cow" });

            Assert.Equal("Blueberry Raspberry Strawberry Fusion", s1.GetName());
        }


    }
}
