using System;
using Xunit;
using Katas.Kata_4;
namespace KataUnitTests
{
    public class BriefCaseLockTests
    {
        [Fact]
        public void MinTurns_Val4089Target5672_shouldReturnInt9() {
            Assert.Equal(9, BriefCaseLock.MinTurns("4089", "5672"));
         }        
        [Fact]
        public void MinTurns_Val1111Target1100_shouldReturnInt2() {
            Assert.Equal(2, BriefCaseLock.MinTurns("1111", "1100"));
         }
        [Fact]
        public void MinTurns_Val2391Target4984_shouldReturnInt10()
        {
            Assert.Equal(10, BriefCaseLock.MinTurns("2391", "4984"));
        }
        [Fact]
        public void MinTurns_Val0000Target1234_shouldReturnInt10()
        {
            Assert.Equal(10, BriefCaseLock.MinTurns("0000", "1234"));
        }
    }
}
