﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Katas.Kata_4
{
    public class BriefCaseLock
    {
        public static int MinTurns(string value, string target)
        {
            if(value.Length != target.Length)
            {
                return -1;
            }
            int turns = 0;                                                                  // init turns
            int[] v = value.Select(x => (int) x).ToArray();                                 // Create Arrays of integers containing each number
            int[] t = target.Select(x => (int) x).ToArray();
            for(int i=0; i<v.Length; i++)                                                   // for loop with length of array
            {
                turns += Math.Min((Math.Abs(v[i] - t[i])), 10 - (Math.Abs(t[i] - v[i])));   // find the minimum turns and increment turns var
            }
            return turns;
        }
    }
}
