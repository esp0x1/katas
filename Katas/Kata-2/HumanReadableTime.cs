﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Katas.Kata_2
{
    public class HumanReadableTime
    {
        public string GetReadableTime(int input)
        {
            if(input < 0 || input > 359999) 
            {
                return "invalid input";
            }
            TimeSpan ts = new(0,0,input); 
            return $"{(int)(ts.TotalHours):00}:{ts.Minutes:00}:{ts.Seconds:00}";

        }
    }
}
