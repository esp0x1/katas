﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Katas.Kata_5
{
    public enum VGNTypes
    {
        Soy,
        Oat,
        Almond
    }
    public class Smoothie
    {
        public Dictionary<string, double> IngredientPrices { get; set; }
        public string[] Ingredients { get; set; }

        public Smoothie(string[] ingredients)
        {
            IngredientPrices = new Dictionary<string, double>()
            {
                {"Strawberry", 1.5 },
                {"Banana", 0.5 },
                {"Mango", 2.5 },
                {"Blueberry", 1 },
                {"Raspberry", 1 },
                {"Apple", 1.75 },
                {"Pineapple", 3.5 },
                {"Almond", 0 },
                {"Soy", 0 },
                {"Oat", 0 },
                {"Cow", 0 }
            };
            Ingredients = ingredients;
        }
        public double GetCost()
        {
            double cost = 0;
            foreach(var ingredient in Ingredients)
            {
                cost += IngredientPrices[ingredient];
            }
            return cost;
        }
        public double GetPrice()
        {
            double cost = GetCost();
            return Math.Round(cost + (cost * 1.5), 2); 
        }
        public string GetName()
        {
            StringBuilder sb = new StringBuilder();
            SortedSet<string> sortedIngredients = new(Ingredients);
            foreach (var ingredient in sortedIngredients)
            {
                if ((Enum.IsDefined(typeof(VGNTypes), ingredient)) || ingredient == "Cow")
                {
                    continue;
                }
                else 
                {
                    sb.Append($"{ingredient} ");
                }
                

            }
            if (sortedIngredients.Count() > 2)
            {
                sb.Append("Fusion");
            }
            else
            {
                sb.Append("Smoothie ");
            }
            if (Enum.IsDefined(typeof(VGNTypes), Ingredients.Last()))
            {
                sb.Append("(VGN)");
            }

            return sb.ToString();
        }


    }
}
