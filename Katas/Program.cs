﻿using System;
using Katas.Kata_0;
using Katas.Kata_1;
using Katas.Kata_2;
using Katas.Kata_3;
using Katas.Kata_6;
namespace Katas
{
    public class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine(PigLatinTranslator.TranslateWord("flag"));
        }
    }
}
