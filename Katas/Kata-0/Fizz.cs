﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Katas.Kata_0
{
    public class Fizz
    {
        public string[] FizzBuzzCalculate(int number)
        {
            String[] output = new string[number];
            // check for multiples of 3,5 and 3 AND 5
            // % modulus operetaor
            // 5 % 2 = 1
            // 4 % 2 = 0
            for (int i = 0; i<number; i++)
            {
                output[i] = FizzBuzzChecker(i+1); 
            }
            Console.WriteLine(output);
            return output;
        }

        private string FizzBuzzChecker(int number)
        {
            if (number % 3 == 0)
            {
                //multiple of 3
                return "fizz";
            }
            else if (number % 5 == 0)
            {
                //multiple of 5
                return "buzz";
            }
            else if (number % 15 == 0)
            {
                //multiple of 15
                return "FizzBuzz";
            }
            // no fizzbuzz number
            return number.ToString();
        }
    }
}
