﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Katas.Kata_1
{
    public class parseltongue
    {
        public bool IsParselTongue(string input)
        {
            if (string.IsNullOrEmpty(input)) return false;

            //split words in string into array of strings
            string[] words = input.Split(' ');
            bool isParsel = true;
            foreach(string word in words)
            {
                string normalizedText = word.ToLower();
                int count = normalizedText.Count(c => c == 's');
                if(normalizedText.Contains("ss")) { isParsel = true; }
                else if (count == 0) { isParsel = true; }
                else { return false;}
            }
            return isParsel;
        }
    }
}
