﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Katas.Kata_3
{
    public class RemoveTheWord
    {
        public string[] RemoveLetters(string[] toEdit, string toRemove)
        {
            List<string> listOfArr = new List<string>(toEdit);
            foreach (char c in toRemove)
            {
                listOfArr.Remove(c.ToString());
            };
            return listOfArr.ToArray();
        }
    }
}
