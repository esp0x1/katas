﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Katas.Kata_6
{
    public class PigLatinTranslator
    {
        public static string TranslateWord(string word)
        {
            // init chars to not move.
            char[] vowels = new[] { 'a', 'e', 'i', 'o', 'u' };
            // chars to set at last index
            char[] specialChars = new[] { '?', ',', '.', '!' };
            //ease the manipulation flow of the string
            List<char> charWord = new List<char>(word);
            // Preserve Punctuation
            Char sChar = '0';
            if (specialChars.Contains(charWord.Last()))
            {
                sChar = charWord.Last();
                charWord.Remove(sChar);
            }

            // move chars which are not vowels
            foreach(char ch in word)
            {
                // if ch is not a vowel, remove it from current index and add at the end.
                // if first char is a vowel, stop the loop.
                if (!vowels.Contains(Char.ToLower(ch))){
                    char toRemove = ch;
                    charWord.Remove(toRemove);
                    charWord.Add(toRemove);
                } else if (vowels.Contains(charWord.First()))
                {
                    break;
                };
            }
            // preserve punctuation || special characters
            if (vowels.Contains(charWord.First()) || word.Length == 1) //yep!
            {
                charWord.Add('a');
                charWord.Add('y');
            }
            if (sChar != '0')
            {
                charWord.Add(sChar);
            }
  
            return new string(charWord.ToArray());
        }

        public static string TranslateSentence(string sentence)
        {
            StringBuilder sb = new();
            string[] words = sentence.Split(' ');
            foreach(var word in words)
            {
                sb.Append($"{TranslateWord(word)} ");
            }
            return sb.ToString();
        }
    }
}
